import { Component, ViewChild, ElementRef } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  @ViewChild('pwaphoto', { static: true }) pwaphoto: ElementRef;
  imageUri: string = null;

  constructor() {}

  openPicker() {
    if (this.pwaphoto == null) {
      return;
    }
    this.pwaphoto.nativeElement.click();
  }

  uploadPhoto() {
    if (this.pwaphoto == null) {
      return;
    }

    const fileList: FileList = this.pwaphoto.nativeElement.files;
    if (fileList && fileList.length > 0) {
      this.getBase64ImageFromUrl(fileList[0]).then(
        (result: string) => {
          this.imageUri = result;
        },
        (err: any) => {
          // Ignore error, do nothing
          this.imageUri = null;
        }
      );
    }
  }

  getBase64ImageFromUrl(imageUri) {
    return new Promise((resolve, reject) => {
      let reader  = new FileReader();
      reader.addEventListener("load", function () {
          resolve(reader.result);
      }, false);

      reader.onerror = () => {
        return reject(this);
      };
      // Takes a Blob or a File from which to read
      reader.readAsDataURL(imageUri);
    })
  }
}
